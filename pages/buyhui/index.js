/**
 * 长春市支点科技有限公司 [ http://www.4006660407.com ]
 * 小程序代码对外公开，以供大家进行参考和使用
 * 小程序中各项界面数据均已模拟，只需要简单的开发即可实际应用
 * 后台代码因版权原因无法公开，请谅解
 * 如需后端代码请咨询：400-666-0407
 * 或搜索微信公众号 zhidianweixin 或 支点Fulcrum
 * 共享茶室无缝对接互联网+时代，
 * 专业解决商务人士稳定而高频的商谈空间需求稀缺的痛点，
 * 符合各大企业外勤人员、小微企业创业 人员、自由职业者、异地差旅人士、小型会议、品茶爱好者等群体的需求。
 * 通过物联网技术实现无人值守的管理系统，
 * 开创全新“低门槛”、“高隐私”、“无营销”，
 * 24小时不打烊智能共享茶室模式。
 * 是您休闲会客、静心的好去处。
 * 已为多家茶馆提供专业管理系统解决方案。
 */
const app = getApp()
// 引入SDK核心类
const QQMapWX = require('../../lib/qqmap-wx-jssdk.min');
const key = 'QYUBZ-AGNC2-ISZUO-CVF3A-PDBUF-E7F2J'; //使用在腾讯位置服务申请的key
// 实例化API核心类
const qqmapsdk = new QQMapWX({
    key: key // 必填
}); 
Page({
  data: {
    type:1, //类型
    userInfo: {},
    cityname:'北京市',
    thiscity:{},
    house:{},
    house_index:0,
    housename:'请选择店面', //店面名称 
    cuset:{}, //充值列表
    cuset_index:0,
    kaset:{}, //办卡列表
    kaset_index:0,
    phone:''
  },
  onLoad: function () {
    var that = this;
    if (app.globalData.userInfo==undefined || JSON.stringify(app.globalData.userInfo)=='{}') {
      app.userInfoReadyCallback = res => {
        that.setData({
          userInfo:app.globalData.userInfo
        });
        that.loadready();
      }
    }else{
      that.setData({
        userInfo:app.globalData.userInfo
      });
      that.loadready();
    }
  },
  loadready: function() {
    var that = this;
    if(JSON.stringify(app.globalData.thiscity) == "{}"){
      //逆地址解析
      qqmapsdk.reverseGeocoder({
        location: '', //获取表单传入的位置坐标,不填默认当前位置,示例为string格式
        success: function(re) {//成功后的回调
          var city = {city: re.result.ad_info.city, lng: re.result.ad_info.location.lng, lat: re.result.ad_info.location.lat};
          that.setData({
            cityname:city.city,
            thiscity:city
          });
          app.globalData.thiscity = city;
          that.showhouse();
        },
        fail: function(error) {
          console.error(error);
        }
      })
    }else{
      var city = app.globalData.thiscity;
      that.setData({
        cityname:city.city,
        thiscity:city
      });
      that.showhouse();
    }
  },
  onShow:function(){
    wx.showShareMenu({
      withShareTicket:true,
      menus:['shareAppMessage','shareTimeline']
    })
    var that = this;
    if(!(JSON.stringify(app.globalData.thiscity) == "{}")){
        var city = app.globalData.thiscity;
        that.setData({
          cityname:city.city,
          thiscity:city
        });
        that.showhouse();
    }
  },
  //展示房间
  showhouse:function(){
    let that = this;
    var house = [{
          "id": 6,
          "name": "支点科技店",
          "tel":'4006660407',
          "cuset": [{
              "id": 1,
              "hid": 6,
              "name": "600元",
              "money": "500.00",
              "zmoney": "600.00",
              "content": "充值500元送100元"
          }, {
              "id": 2,
              "hid": 6,
              "name": "1200元",
              "money": "1000.00",
              "zmoney": "1200.00",
              "content": "充值1000元送200元"
          }, {
              "id": 3,
              "hid": 6,
              "name": "6000元",
              "money": "5000.00",
              "zmoney": "6000.00",
              "content": "充值5000元送1000元"
          }],
          "kaset": [{
                  "id": 1,
                  "hid": 6,
                  "name": "年卡",
                  "days": 365,
                  "hours": 2,
                  "money": "19800.00",
                  "content": "每天可享受2小时休闲时光"
              },
              {
                "id": 2,
                "hid": 6,
                "name": "季卡",
                "days": 90,
                "hours": 2,
                "money": "6880.00",
                "content": "每天可享受2小时休闲时光"
            },{
              "id": 3,
              "hid": 6,
              "name": "月卡",
              "days": 30,
              "hours": 2,
              "money": "2800.00",
              "content": "每天可享受2小时休闲时光"
          }]
      }];
      that.setData({
        house:house,
        housename:house.length>0?'请选择店面':'当前城市暂无店面',
      });
  },
  //支付
  payment:function(){
    var that = this;
    var info = that.data.paydata;
    var type = that.data.type;
    if(type==1){ //储值
      var selval = that.data.cuset[that.data.cuset_index];
    }else{
      var selval = that.data.kaset[that.data.kaset_index];
    }
    //判断服务是否选择
    if(JSON.stringify(selval)==undefined){
      wx.showToast({
        title: '请选择要购买的服务',
        icon: 'none',
        duration: 2000
      })
      return false;
    }
    //生成订单并支付下单
    wx.requestPayment({
      "timeStamp": '13213131312',
      "nonceStr": '2123123',
      "package": '213123123',
      "signType": "MD5",
      "paySign": '65654654654',
      "success":function(res){
        wx.navigateBack();
      },
      "fail":function(res){
        wx.showToast({
          title: '支付失败',
          icon: 'none',
          duration: 2000,
          complete:function(){
            setTimeout(function () {
              wx.navigateBack();
            }, 2000)
          }
        })
      },
      "complete":function(res){
        //console.log(3);
      }
    })
  },
  changetype:function(option){
    var that = this;
    var type = option.currentTarget.id
    that.setData({type: type});
  },
  xuanCuset:function(e){
    this.setData({cuset_index: e.currentTarget.id});
  },
  xuanKaset:function(e){
    this.setData({kaset_index: e.currentTarget.id});
  },
  calltel:function(e){
    wx.makePhoneCall({
      phoneNumber: this.data.phone
    })
  },
  houseSel:function(e){
    var that = this;
    if(that.data.house.length<=0){
      wx.showToast({
        title: '当前城市暂无店面，请切换城市',
        icon: 'none',
        duration: 2000
      })
    }else{
      that.setData({
        house_index:e.detail.value,
        housename:that.data.house[e.detail.value]['name'],
        cuset:that.data.house[e.detail.value]['cuset'], //充值列表
        kaset:that.data.house[e.detail.value]['kaset'], //办卡列表
        phone:that.data.house[e.detail.value]['tel']
      });
    }
  },
  //前往城市选择
  gotoSelCity:function(){
    wx.navigateTo({
      url: '/pages/selcity/index'
    })
  },
  onShareAppMessage: function (res) {
    return {
      title: '我在共享茶室等你',
      path: '/pages/map/index',
      imageUrl:'/image/share.jpg'
    }
  }
})