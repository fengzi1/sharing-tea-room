/**
 * 长春市支点科技有限公司 [ http://www.4006660407.com ]
 * 小程序代码对外公开，以供大家进行参考和使用
 * 小程序中各项界面数据均已模拟，只需要简单的开发即可实际应用
 * 后台代码因版权原因无法公开，请谅解
 * 如需后端代码请咨询：400-666-0407
 * 或搜索微信公众号 zhidianweixin 或 支点Fulcrum
 * 共享茶室无缝对接互联网+时代，
 * 专业解决商务人士稳定而高频的商谈空间需求稀缺的痛点，
 * 符合各大企业外勤人员、小微企业创业 人员、自由职业者、异地差旅人士、小型会议、品茶爱好者等群体的需求。
 * 通过物联网技术实现无人值守的管理系统，
 * 开创全新“低门槛”、“高隐私”、“无营销”，
 * 24小时不打烊智能共享茶室模式。
 * 是您休闲会客、静心的好去处。
 * 已为多家茶馆提供专业管理系统解决方案。
 */
const app = getApp();
// 引入SDK核心类
const QQMapWX = require('../../lib/qqmap-wx-jssdk.min');
const key = 'QYUBZ-AGNC2-ISZUO-CVF3A-PDBUF-E7F2J'; //使用在腾讯位置服务申请的key
// 实例化API核心类
const qqmapsdk = new QQMapWX({
    key: key // 必填
}); 
Page({
  data: {
    thiscity:{},
    minkm:'0m',
    house:[],
    ishouse:'0',
    driving:[],
    opset:[],
    banner:[],
    moren:'加载中...'
  },
  onLoad: function () {
      var that = this;
      that.setData({
        thiscity:app.globalData.thiscity,
        minkm:app.globalData.minkm
      });
      //查询附近茶室
      that.findTeaRoom();
      //获取焦点图
      that.setData({banner:app.globalData.benner});
  },
  onShow:function(){
    wx.showShareMenu({
      withShareTicket:true,
      menus:['shareAppMessage','shareTimeline']
    })
  },
  //前往房间详情页面
  goroomcon:function(option){
    var that = this;
    var getid = option.currentTarget.id;
    var nar = getid.split(",");
    var dr = that.data.driving[nar[1]];
    app.globalData.dr = dr;
    wx.navigateTo({
      url: '/pages/content/index?id='+nar[0]+'&pid='+nar[1]
    })
  },
  findTeaRoom:function(){
    let that = this;
    let house = [{
          "id": 7,
          "name": "支点科技店",
          "picture": "/image/banner00.jpg",
          "address": "支点科技",
          "yytime": "00:00 ~ 24:00",
          "lng": 110.196109,
          "lat": 20.045091,
          "tel": "4006660407",
          "room": [{
              "id": 28,
              "pid": 7,
              "name": "星辰大海",
              "ymoney": "200.00",
              "money": "100",
              "num": 18,
              "renshu": "1",
              "qiding": "2",
              "pic": "/image/cs01.jpg"
          }]
      }
    ];
    //如果没有茶室信息
    if(house.length>0){
      var opset = [];
      house.forEach(function(val,key){
          //获取驾车距离
          that.getMapDriving([that.data.thiscity.lng,that.data.thiscity.lat,val.lng,val.lat],val.id);
          //获取直线距离
          house[key]['s'] = that.getMapDistanceApi([that.data.thiscity.lng,that.data.thiscity.lat,val.lng,val.lat]);
          opset[val.id] = 0;
      });
      //根据直线距离进行排序
      house.sort(function(x, y){
        return x['s'] - y['s'];
      });
      that.setData({ishouse:'1',house:house,opset:opset});
    }else{
      that.setData({moren:'当前城市无可用茶室'});
    }
  },
  //设置
  opSet:function(option){
    var that = this;
    var opset = that.data.opset;
    var pid = option.currentTarget.dataset.pid;
    var newopset = [];
    opset.forEach(function(val,key){
        if(key==pid){
          newopset[key] = val==0?1:0;
        }else{
          newopset[key] = 0;
        }
    });
    that.setData({opset:newopset});
  },
  // 两点间距离lat1,  lng1,  lat2,  lng2
  getMapDistanceApi:function(paramObj){
    var lng1 = paramObj[0];
    var lat1 = paramObj[1];
    var lng2 = paramObj[2];
    var lat2 = paramObj[3];
    var radLat1 = lat1*Math.PI / 180.0;
    var radLat2 = lat2*Math.PI / 180.0;
    var a = radLat1 - radLat2;
    var b = lng1*Math.PI / 180.0 - lng2*Math.PI / 180.0;
    var s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a/2),2) + Math.cos(radLat1)*Math.cos(radLat2)*Math.pow(Math.sin(b/2),2)));
    s = s * 6378.137 ;
    s = Math.round(s * 10000) / 10000;
    s = s * 1000;
    if (isNaN(s)) {
      return 0;
    }else{
      return s;
    }
  },
  //获取地点实际路线距离和驾车时间
  getMapDriving:function(e,id) {
    var _this = this;
    var juli = 0;
    var time = 0;
    //调用距离计算接口
    qqmapsdk.direction({
      mode: 'driving',//可选值：'driving'（驾车）、'walking'（步行）、'bicycling'（骑行），不填默认：'driving',可不填
      //from参数不填默认当前地址
      from: {latitude: e[1],longitude: e[0]},
      to: {latitude: e[3],longitude: e[2]}, 
      success: function (res) {
        var ret = res.result.routes[0];
        juli = ret.distance;
        time = ret.duration;
        if (time > 60) {
          //大于一小时
          time = Math.floor(time/60 * 100) / 100;
          time = time.toFixed(2)*1 + '小时';
        } else {
          //小于一小时
          time = time.toFixed(2)*1 + '分钟';
        }
        if (juli > 1000) {
          //大于1000米时
          juli = Math.floor(juli/1000 * 100) / 100;
          juli = juli.toFixed(2)*1 + 'km';
        } else {
          //小于1000米直接返回
          juli = juli.toFixed(2)*1 + 'm';
        }
        var driving = _this.data.driving;
        driving[id] = {time:time,juli:juli};
        _this.setData({driving:driving});
      },
      fail: function (error) {
        console.error(error);
      }
    });
  },
  //前往城市选择
  gotoSelCity:function(){
    wx.navigateBack();
  },
  onShareAppMessage: function (res) {
    return {
      title: '我在共享茶室等你',
      path: '/pages/map/index',
      imageUrl:'/image/share.jpg'
    }
  },
  onShareTimeline: function () {
    return {
      title: '我在共享茶室等你',
      query: '/pages/map/index',
      imageUrl:'/image/share.jpg'
    }
  }
})
