/**
 * 长春市支点科技有限公司 [ http://www.4006660407.com ]
 * 小程序代码对外公开，以供大家进行参考和使用
 * 小程序中各项界面数据均已模拟，只需要简单的开发即可实际应用
 * 后台代码因版权原因无法公开，请谅解
 * 如需后端代码请咨询：400-666-0407
 * 或搜索微信公众号 zhidianweixin 或 支点Fulcrum
 * 共享茶室无缝对接互联网+时代，
 * 专业解决商务人士稳定而高频的商谈空间需求稀缺的痛点，
 * 符合各大企业外勤人员、小微企业创业 人员、自由职业者、异地差旅人士、小型会议、品茶爱好者等群体的需求。
 * 通过物联网技术实现无人值守的管理系统，
 * 开创全新“低门槛”、“高隐私”、“无营销”，
 * 24小时不打烊智能共享茶室模式。
 * 是您休闲会客、静心的好去处。
 * 已为多家茶馆提供专业管理系统解决方案。
 */
//获取应用实例
const app = getApp()
Page({
  data: {
    userInfo: {},
    isloading:false,
    erweima: '',
  },
  onLoad: function (option) {
    var option = option;
    wx.showLoading({
        title: '加载中',
    })
    var that = this;
    if (app.globalData.userInfo==undefined || JSON.stringify(app.globalData.userInfo)=='{}') {
      app.userInfoReadyCallback = res => {
        that.setData({
          userInfo:app.globalData.userInfo
        });
        that.loadready(option);
      }
    }else{
      that.setData({
        userInfo:app.globalData.userInfo
      });
      that.loadready(option);
    }
  },
  onShow:function(){
    wx.showShareMenu({
      withShareTicket:true,
      menus:['shareAppMessage','shareTimeline']
    })
  },
  //启动函数
  loadready:function(option){
    var that = this;
    that.setData({
      erweima: '/image/code.jpg',
      isloading:true,
    }); 
    wx.hideLoading();
  },
  onShareAppMessage: function (res) {
    return {
      title: '我在共享茶室等你',
      path: '/pages/map/index',
      imageUrl:'/image/share.jpg'
    }
  },
  onShareTimeline: function () {
    return {
      title: '我在共享茶室等你',
      query: '/pages/map/index',
      imageUrl:'/image/share.jpg'
    }
  }
})
