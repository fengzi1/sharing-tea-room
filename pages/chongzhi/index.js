/**
 * 长春市支点科技有限公司 [ http://www.4006660407.com ]
 * 小程序代码对外公开，以供大家进行参考和使用
 * 小程序中各项界面数据均已模拟，只需要简单的开发即可实际应用
 * 后台代码因版权原因无法公开，请谅解
 * 如需后端代码请咨询：400-666-0407
 * 或搜索微信公众号 zhidianweixin 或 支点Fulcrum
 * 共享茶室无缝对接互联网+时代，
 * 专业解决商务人士稳定而高频的商谈空间需求稀缺的痛点，
 * 符合各大企业外勤人员、小微企业创业 人员、自由职业者、异地差旅人士、小型会议、品茶爱好者等群体的需求。
 * 通过物联网技术实现无人值守的管理系统，
 * 开创全新“低门槛”、“高隐私”、“无营销”，
 * 24小时不打烊智能共享茶室模式。
 * 是您休闲会客、静心的好去处。
 * 已为多家茶馆提供专业管理系统解决方案。
 */
//获取应用实例
const app = getApp()
Page({
  data: {
    userInfo: {},
    isloading:false,
    lists:[{
      hname:'支点科技店',
      address:'支点科技',
      money:'1000',
      smoney:'200',

    }],//列表
    page:1, //分页
    tsmsg:false, //底部提示信息是否显示
    bottommsg:"加载中...",
    listurl:'getList', //获取列表API
    islistnum:true
  },
  onLoad: function () {
    wx.showLoading({
        title: '加载中',
    })
    var that = this;
    if (app.globalData.userInfo==undefined || JSON.stringify(app.globalData.userInfo)=='{}') {
      app.userInfoReadyCallback = res => {
        that.setData({
          userInfo:app.globalData.userInfo
        });
        //that.loadready();
        wx.hideLoading();
      }
    }else{
      that.setData({
        userInfo:app.globalData.userInfo
      });
      //that.loadready();
      wx.hideLoading();
    }
  },
  onShow:function(){
    wx.showShareMenu({
      withShareTicket:true,
      menus:['shareAppMessage','shareTimeline']
    })
  },
  loadready:function(){
    var that = this;
    wx.request({
      url: app.globalData.httpUrl+'/api/'+that.data.listurl+'.html',
      data:{openid:that.data.userInfo.openid,page:1},
      success (res) {
        if(res.data.res==1){
          that.setData({
            lists: res.data.list,
            page:1,
            bottommsg: "加载中...",
            tsmsg:false,
            islistnum:res.data.num>0?true:false
          });
          wx.hideLoading();
          wx.stopPullDownRefresh();
        }else{
          wx.hideLoading();
        }
      },
      error(err) {
        console.log(err);
      }
    });
  },
  //下拉刷新
  onPullDownRefresh:function(){
    //this.loadready();
    wx.stopPullDownRefresh(); //停止下拉刷新
  },
  //监听触底
  onReachBottom:function(){
    var that = this;
    var page = that.data.page+1;
    var list = that.data.list;
    if(!that.data.tsmsg){
      that.setData({
        bottommsg: "加载中...",
        tsmsg:true
      });
      wx.request({
        url: app.globalData.httpUrl+'/api/'+that.data.listurl+'.html',
        data:{openid:that.data.userInfo.openid,page:page},
        success (res) {
          if(res.data.res==1){
            if(res.data.num>0){
              that.setData({
                lists: list.push(res.data.list),
                tsmsg:false
              });
            }else{
              that.setData({
                bottommsg: "没有更多了",
              });
            }
            wx.hideLoading(); //隐藏加载
            wx.stopPullDownRefresh(); //停止下拉刷新
          }else{
            wx.hideLoading();
          }
        },
        error(err) {
          console.log(err);
        }
      });
    }
  },
  onShareAppMessage: function (res) {
    return {
      title: '我在共享茶室等你',
      path: '/pages/map/index',
      imageUrl:'/image/share.jpg'
    }
  },
  onShareTimeline: function () {
    return {
      title: '我在共享茶室等你',
      query: '/pages/map/index',
      imageUrl:'/image/share.jpg'
    }
  }
})