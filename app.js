//app.js
/**
 * 长春市支点科技有限公司 [ http://www.4006660407.com ]
 * 小程序代码对外公开，以供大家进行参考和使用
 * 小程序中各项界面数据均已模拟，只需要简单的开发即可实际应用
 * 后台代码因版权原因无法公开，请谅解
 * 如需后端代码请咨询：400-666-0407
 * 或搜索微信公众号 zhidianweixin 或 支点Fulcrum
 * 共享茶室无缝对接互联网+时代，
 * 专业解决商务人士稳定而高频的商谈空间需求稀缺的痛点，
 * 符合各大企业外勤人员、小微企业创业 人员、自由职业者、异地差旅人士、小型会议、品茶爱好者等群体的需求。
 * 通过物联网技术实现无人值守的管理系统，
 * 开创全新“低门槛”、“高隐私”、“无营销”，
 * 24小时不打烊智能共享茶室模式。
 * 是您休闲会客、静心的好去处。
 * 已为多家茶馆提供专业管理系统解决方案。
 */
App({
  onLaunch: function () {
    var that = this;
    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          wx.login({
            success: res => {
              wx.getUserInfo({
                success: rs => {
                    that.globalData.userInfo = {
                      "openid": "1111",
                      "avatar": "/image/gr01.png",
                      "nickname": "支点科技",
                      "gender": 1,
                      "province": "Jilin",
                      "city": "Changchun",
                      "session_key": "",
                      "phone": "4006660407",
                      "isvip": 0
                  };
                  if (that.userInfoReadyCallback) {
                    that.userInfoReadyCallback(re.data)
                  }
                }
              })
            }
          })
        }else{
          wx.navigateTo({
            url: '/pages/empower/index'
          })
        }
      }
    })
  },
  globalData: {
    //会员信息
    userInfo: null,
    //接口URL地址以及图片展现地址
    httpUrl:'https://',
    //所有城市的信息
    allcity:{},
    //所有茶室的点位表
    markers:{},
    //当前选择的城市信息，包含所选择的城市名称 和 选择的坐标 city lat lng
    thiscity:{},
    //当前位置距离的最近的茶室
    minkm:'0m',
    //当前所选择的距离和路程驾车时间，包含 juli 和 time
    dr:[],
    //焦点图
    benner:[{
          "title": "焦点图1",
          "picture": "/image/banner00.jpg",
          "link": "#"
      },{
        "title": "焦点图2",
        "picture": "/image/banner01.jpg",
        "link": "#"
    }],
    //支付信息
    payinfo:{},
    //配置信息
    setting:{kefu_tel: "400-666-0407", meng_tel: "400-666-0407"}
  }
})